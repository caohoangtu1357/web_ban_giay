var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose=require('mongoose');
var bodyParser=require('body-parser');
var session =require('express-session');
var passport= require('passport');
var app = express();
var indexRouter=require('./routes/index.router');
var fs = require('fs');
var layouts= require('express-ejs-layouts');
var upload=require('express-fileupload');

require('./config/passport.js')(passport);

app.set("views",path.join(__dirname,"views"));
app.set("view engine","ejs");
app.use(express.static(path.join(__dirname, "public")));


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(upload());

app.set('layout','admin/layout');
app.use(layouts);

// Express session
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);
app.use(passport.initialize());
app.use(passport.session());

//body-parser
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//Main Route
app.use('/', indexRouter);



//connect mongodb
mongoose.connect('mongodb://localhost:27017/web_ban_giay',{ autoIndex: true },(err)=>{
    if(err){
        console.log(err);
    }
    else{
        console.log('connected to mongodb');
    }
});

app.listen(8080,()=>{
  console.log("server is running on port 8080");
});
//3ece671cc0018e772a56acc34932adae

module.exports = app;

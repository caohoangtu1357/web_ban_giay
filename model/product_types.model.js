const productTypeSchema=require('../schema/product_types.schema');


function setProductType(name) { 
    return new Promise((resolve,reject)=>{
        var productType=new productTypeSchema();
        productType.name=name;
        productType.save().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        });
    });    
}

function get(id) {
    return new Promise((resolve,reject)=>{
        productTypeSchema.findOne({_id:id}).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        });
    });
}

function gets() {
    return new Promise((resolve,reject)=>{
        productTypeSchema.find().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        });
    });
}

function updateProductType(id,name){
    return new Promise((resolve,reject)=>{
        productTypeSchema.findOneAndUpdate({_id:id},{name:name}).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        });
    });
}

function deleteProductType(id){
    return new Promise((resolve,reject)=>{
        productTypeSchema.deleteOne({_id:id}).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

module.exports = {
    setProductType:setProductType,
    gets:gets,
    get:get,
    updateProductType:updateProductType,
    deleteProductType:deleteProductType
}; 
const bcrypt=require('bcryptjs');
const userSchema=require('../schema/users.schema');


function setUser(name,email,password,phone,address,description) {       
    
    return new Promise((resolve,reject)=>{
        var user=new userSchema();
        user.name=name;
        user.email=email;
        user.password=password;
        user.phone=phone;
        user.address=address;
        user.description=description;
        return bcrypt.hash(password,10)
            .then(hashed=>{
                user.password=hashed;
                    return user.save().
                        then(user=>{
                            return resolve(user);
                        })
                        .catch(err=>{
                            return reject(err);
                        });
            })
    });

}

function gets() {
    return product.find();
}

function update(id,name,email,password,phone,address,description){
    return product.findOneAndUpdate({id:id},{name:name,email:email,password:password,phone:phone,address:address,description:description});
}

function deleteUser(id){
    return product.deleteOne({id:id});
}

module.exports = {
    setUser:setUser,
    gets:gets,
    update:update,
    deleteUser:deleteUser
}; 
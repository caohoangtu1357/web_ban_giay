const productSchema=require('../schema/products.schema');


function setProduct(name,price,promotion,sizes,status,product_type_id,gender,description,thumbnail,producer_id,discount) {       
    return new Promise((resolve,reject)=>{
        var product=new productSchema();
        product.name=name;
        product.price=price;
        product.promotion=promotion;
        product.sizes=sizes;
        product.status=status;
        product.product_type_id=product_type_id;
        product.gender=gender;
        product.description=description;
        product.thumbnail=thumbnail;
        product.discount=discount;
        product.producer_id=producer_id;
        product.save().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

function get(id){
    return new Promise((resolve,reject)=>{
        productSchema.findOne({_id:id}).then(data=>{
            return resolve(data)
        }).catch(err=>{
            return reject(err)
        })
    })
}

function gets() {
    return new Promise((resolve,reject)=>{
        productSchema.find().then(data=>{
            return resolve(data)
        }).catch(err=>{
            return reject(err)
        })
    })
}

function updateProduct(id,name,price,promotion,sizes,status,product_type_id,gender,description,thumbnail,producer_id,discount){
    return new Promise((resolve,reject)=>{
        productSchema.findOneAndUpdate({_id:id},{
            name:name,
            price:price,
            promotion:promotion,
            sizes:sizes,
            status:status,
            product_type_id:product_type_id,
            gender:gender,
            description:description,
            thumbnail:thumbnail,
            discount:discount,
            producer_id:producer_id
        
        }).then(data=>{
            return resolve(data)
        }).catch(err=>{
            return reject(err)
        })
    })
}

function deleteProduct(id){
    return new Promise((resolve,reject)=>{
        productSchema.deleteOne({_id:id}).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

function getProductsByIdProductType(idProductType){
    return new Promise((resolve,reject)=>{
        productSchema.find({product_type_id:idProductType}).then(products=>{
            resolve(products);
        }).catch(err=>{
            reject(err);
        });
    });
}

function getProductsByIdProducer(idProducer){
    return new Promise((resolve,reject)=>{
        productSchema.find({producer_id:idProducer}).then(products=>{
            resolve(products);
        }).catch(err=>{
            reject(err);
        });
    });
}

function deleteAnThumbnail(idProduct,thumbnail){
    return new Promise((resolve,reject)=>{
        productSchema.update({_id:idProduct},{$pullAll:{thumbnail: [thumbnail]}}).then(data=>{
            resolve(data);
        }).catch(err=>{
            reject(err);
        });
    });
}

module.exports = {
    setProduct:setProduct,
    get:get,
    gets:gets,
    updateProduct:updateProduct,
    deleteProduct:deleteProduct,
    getProductsByIdProductType:getProductsByIdProductType,
    getProductsByIdProducer:getProductsByIdProducer,
    deleteAnThumbnail:deleteAnThumbnail
}; 
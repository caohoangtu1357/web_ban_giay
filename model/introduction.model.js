const introductionSchema=require('../schema/introductions.schema');


function setIntroduction(name,description,phone,social_link) {       
    return new Promise((resolve,reject)=>{
        var introduction=new introductionSchema();
        introduction.name=name;
        introduction.description=description;
        introduction.phone=phone;
        introduction.social_link=social_link;
        introduction.save().then(data=>{
            return resolve(data)
        }).catch(err=>{
            return reject(err)
        });
    })
}

function gets() {
    return new Promise((resolve,reject)=>{
        introductionSchema.find().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(data);
        })
    });
}

function updateIntroduction(id,name,description,phone,social_link){
    return new Promise((resolve,reject)=>{
        introductionSchema.findOneAndUpdate({_id:id},{name:name,description:description,phone:phone,social_link:social_link}).then(data=>{
            return resolve(data)
        }).catch(err=>{
            return reject(err)
        })
    });
}

function deleteIntroduction(id){
    return introductionSchema.deleteOne({_id:id}).then(data=>{
        return resolve(data);
    }).catch(err=>{
        return reject(err);
    })
}

module.exports = {
    setIntroduction:setIntroduction,
    gets:gets,
    updateIntroduction:updateIntroduction,
    deleteIntroduction:deleteIntroduction
}; 
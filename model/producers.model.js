const producerSchema=require('../schema/producers.schema');


function setProducer(name) {      
    return new Promise((resolve,reject)=>{
        var producer=new producerSchema();
        producer.name=name;
        producer.save().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err)
        })
    });
}

function gets() {
    return new Promise((resolve,reject)=>{
        producerSchema.find().then(producers=>{
            return resolve(producers);
        }).catch(err=>{
            return reject(err);
        });
    });
    
}

function update(id,name){
    return new Promise((resolve,reject)=>{
        producerSchema.findOneAndUpdate({_id:id},{name:name}).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(data);
        })
    })
};

function deleteProducer(id){
    return new Promise((resolve,reject)=>{
        producerSchema.deleteOne({_id:id}).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

function getProducer(id){
    return new Promise((resolve,reject)=>{
        producerSchema.find({_id:id}).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}


module.exports = {
    setProducer:setProducer,
    gets:gets,
    update:update,
    deleteProducer:deleteProducer,
    getProducer:getProducer
};
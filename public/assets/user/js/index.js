/*****  slide *****/

let countSlide=0;

function runSlide(){
    let slide=document.getElementsByClassName("slide");
    for(let i=0;i<slide.length;i++){
        slide[i].style.display="none";
    }

    if(countSlide===slide.length){
        countSlide=0;
    }
    slide[countSlide].style.display="block";
    countSlide++;
}

setInterval(runSlide,4000);

/******     loading-product-new ******/ 

let countNew=0;
let myNew=document.getElementsByClassName("new");  
document.getElementById("click-new").addEventListener("click",clickNew);

defaultNew();

function defaultNew(){   
    for(let i=0;i<myNew.length;i++){
        myNew[i].style.display="none";
    }
    for(let j=0;j<4;j++){
        myNew[j].style.display="inline-block"; 
    }
}

function clickNew(){
    if(countNew===myNew.length)
        return;
    countNew+=4;
    for(let j=countNew;j<myNew.length&&j<countNew+4;j++){
        myNew[j].style.display="inline-block"; 
    }
}

/********      ADIDAS  ********/ 
/******      convert-type-adidas     ******/


let myAdidas=document.getElementsByClassName("adidas");
let myNameTypeAdidas=document.getElementsByClassName("name-type-adidas");
let numBeforClickAdidas=0;

defaultConvertAdidas();

function defaultConvertAdidas(){
    for(let i=0;i<myAdidas.length;i++){
        myAdidas[i].style.display="none";
    }
    myAdidas[0].style.display="block";
    myNameTypeAdidas[0].style.color="#BC0000";
    myNameTypeAdidas[0].style.cursor="auto";
}

function clickAdidas(num){
    // set default
    myAdidas[numBeforClickAdidas].style.display="none";
    myNameTypeAdidas[numBeforClickAdidas].style.color="black";
    myNameTypeAdidas[numBeforClickAdidas].style.cursor="pointer";

    // after click 
    myAdidas[num].style.display="block";
    myNameTypeAdidas[num].style.color="#BC0000";
    myNameTypeAdidas[num].style.cursor="auto";

    //set again numBeforClickAdidas
    numBeforClickAdidas=num;
}

/******     loading-product-adidas ******/ 
/* product-adidas-yeezy */
let countYeezy=0;
let myYeezy=document.getElementsByClassName("yeezy");  
document.getElementById("click-yeezy").addEventListener("click",clickYeezy);

defaultYeezy();

function defaultYeezy(){
    
    for(let i=0;i<myYeezy.length;i++){
        myYeezy[i].style.display="none";
    }
    for(let j=0;j<4;j++){
        myYeezy[j].style.display="inline-block";
    }
}

function clickYeezy(){
    if(countYeezy===myYeezy.length)
        return;
    countYeezy+=4;
    for(let j=countYeezy;j<myYeezy.length&&j<countYeezy+4;j++){
        myYeezy[j].style.display="inline-block";
    }
}

/* product-adidas-alaphabounce */
let countAla=0;
let myAla=document.getElementsByClassName("alaphabounce");  
document.getElementById("click-alaphabounce").addEventListener("click",clickAla);

defaultAla();

function defaultAla(){
    
    for(let i=0;i<myAla.length;i++){
        myAla[i].style.display="none";
    }
    for(let j=0;j<4;j++){
        myAla[j].style.display="inline-block";
    }
}

function clickAla(){
    if(countAla===myAla.length)
        return;
    countAla+=4;
    for(let j=0;j<myAla.length&&j<countAla+4;j++){
        myAla[j].style.display="inline-block";
    }
}


/* product-adidas-ultra boost */
let countUlt=0;
let myUlt=document.getElementsByClassName("ultra-boost");  
document.getElementById("click-ultra-boost").addEventListener("click",clickUlt);

defaultUlt();

function defaultUlt(){
    
    for(let i=0;i<myUlt.length;i++){
        myUlt[i].style.display="none";
    }
    for(let i=0;i<4;i++){
        myUlt[i].style.display="inline-block";
    }
}

function clickUlt(){
    if(countUlt===myUlt.length)
        return;
    countUlt+=4;
    for(let j=countUlt;j<myUlt.length&&j<countUlt+4;j++){
        myUlt[j].style.display="inline-block";
    }
}

/********      NIKE       ********/ 
/******      convert-type-nike     ******/


let myNike=document.getElementsByClassName("nike");
let myNameTypeNike=document.getElementsByClassName("name-type-nike");
let numBeforClickNike=0;

defaultConvertNike();

function defaultConvertNike(){
    for(let i=0;i<myNike.length;i++){
        myNike[i].style.display="none";
    }
    myNike[0].style.display="block";
    myNameTypeNike[0].style.color="#BC0000";
    myNameTypeNike[0].style.cursor="auto";
}

function clickNike(num){
    // set default
    myNike[numBeforClickNike].style.display="none";
    myNameTypeNike[numBeforClickNike].style.color="black";
    myNameTypeNike[numBeforClickNike].style.cursor="pointer";

    // after click 
    myNike[num].style.display="block";
    myNameTypeNike[num].style.color="#BC0000";
    myNameTypeNike[num].style.cursor="auto";

    //set again numBeforClickNike
    numBeforClickNike=num;
}

/******     loading-product-Nike ******/ 
/* product-nike-air-max-97 */
let countAirMax97=4;
let myAirMax97=document.getElementsByClassName("air-max-97");  
document.getElementById("click-air-max-97").addEventListener("click",clickAirMax97);

defaultAirMax97();

function defaultAirMax97(){
    
    for(let i=0;i<myAirMax97.length;i++){
        myAirMax97[i].style.display="none";
    }
    for(let i=0;i<myAirMax97.length&&i<4;i++){
        myAirMax97[i].style.display="inline-block";
    }
}

function clickAirMax97(){
    if(countAirMax97===myAirMax97.length)
        return;
    countAirMax97+=4;
    for(let i=0;i<myAirMax97.length&&i<countAirMax97;i++){
        myAirMax97[i].style.display="inline-block";
    }
}

/* product-nike-uptempo */
let countUptempo=0;
let myUptempo=document.getElementsByClassName("uptempo");  
document.getElementById("click-uptempo").addEventListener("click",clickUptempo);

defaultUptempo();

function defaultUptempo(){ 
    for(let i=0 ; i < myUptempo.length ; i++){
        myUptempo[ i ].style.display = "none";
    }
    for(let i=0 ; i < myUptempo.length && i < 4 ; i++){
        myUptempo[ i ].style.display = "inline-block";
    }
}

function clickUptempo(){

    if( countUptempo === myUptempo.length )
    
        return ;
    countUptempo+=4;
    for(let i=0 ; i < myUptempo.length && i < countUptempo + 4 ; i++){
        myUptempo[i].style.display = "inline-block";
    }
    
}


/* product-adidas-Air force */
let countAirForce=0;
let myAirForce=document.getElementsByClassName("air-force");  
document.getElementById("click-air-force").addEventListener("click",clickAirForce);

defaultAirForce();

function defaultAirForce(){
    
    for(let i=0;i<myAirForce.length;i++){
        myAirForce[i].style.display="none";
    }
    for(let i=0 ; i < myAirForce.length && i < 4 ; i++){
        myAirForce[i].style.display = "inline-block";
    }
    
}

function clickAirForce(){
    if(countAirForce === myAirForce.length)
        return;
    countAirForce +=4;
    for(let i=0;i<myAirForce.length&&i<countAirForce+4;i++){
        myAirForce[i].style.display = "inline-block";
    }
}


//click-bar

document.getElementById("bar").addEventListener("click",clickBar);
let mobile=document.getElementById("mobile-menu");

function clickBar(){
    mobile.style.transform="translateX(0)";
    mobile.style.transition="transform 0.5s ease";
    document.getElementById("click-space").style.display="block";
}

document.getElementById("click-space").addEventListener("click", clickCloseBar );

function clickCloseBar(){
    mobile.style.transform="translateX(-271px)";
    document.getElementById("click-space").style.display="none";
}


//click mo-memnu

const moMenuDrop = document.getElementsByClassName("mo-menu-iner-into");
const iconMobileMenu=document.getElementsByClassName("mobile-menu");
const iconCloseMobileMenu=document.getElementsByClassName("close-mobile-menu");

function clickMobileMenuDrop(number){
    moMenuDrop[number].style.height="auto";
    moMenuDrop[number].style.transition="height 0.5s ease";
    iconCloseMobileMenu[number].style.opacity="1";
    iconCloseMobileMenu[number].style.zIndex="99";

    iconMobileMenu[number].style.opacity="0";
    iconMobileMenu[number].style.zIndex="-5";
};

function clickCloseMenuDrop(number){
    moMenuDrop[number].style.height="0";
    moMenuDrop[number].style.transition="height 0.5s ease";

    iconMobileMenu[number].style.opacity="1";
    iconMobileMenu[number].style.zIndex="99";

    iconCloseMobileMenu[number].style.opacity="0";
    iconCloseMobileMenu[number].style.zIndex="-5";
}
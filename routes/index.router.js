const Router=require('express').Router();

Router.use('/admin',require('./admin/index.router.js'));

Router.use('/',require('./user/index.router.js'));
module.exports=Router;
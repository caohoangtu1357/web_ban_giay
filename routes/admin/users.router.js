var Router = require('express').Router();
const userController=require('../../controller/user.controller');
const passport= require('passport');
var checkLogged= require('../../middleware/checkLogged.middleware');

Router.post('/login', passport.authenticate('local'),(req,res,next)=>{
    res.json({status:"success"})
});


Router.post('/register',function(req,res,next){
    let userData= req.body;
    userController.postAddUser(userData)
    .then(userAdded=>{
        return res.json({status:"ok"});
    }).catch(err=>{
        return res.json({status:err});
    });
});

Router.get('/logout',(req,res,next)=>{
    req.logout();
    res.json({status:"logged out"});
});
Router.get('/',(req,res,next)=>{
    res.send("Hello World");
});


module.exports = Router;

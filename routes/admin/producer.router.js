const Router= require('express').Router();

const producerController=require('../../controller/producer.controller');

Router.get('/producer-list',renderProducerListPage);

Router.post('/add-producer',(req,res)=>{
    let userData= req.body;
    producerController.createProducer(userData).then(data=>{
        return res.send(data);
    }).catch(err=>{
        return res.send(err);
    });
})

Router.get('/get-producers',(req,res)=>{

    producerController.getProducers().then(data=>{
        return res.send(data);
    }).catch(err=>{
        res.send(err);
    });
   
    
});

Router.get('/get-producer/:id',(req,res)=>{
    let userData=req.body;

    producerController.getProducer(req.params.id).then(data=>{
        return res.send(data);
    }).catch(err=>{
        return res.send(err);
    });
    
})

Router.delete('/delete-producer/:id',(req,res)=>{
    let userData=req.body;
    producerController.deleteProducer(req.params.id).then(data=>{
        res.send(data);
    }).catch(err=>{
        res.send(err);
    });
});

Router.put('/update-producer/:id',(req,res)=>{
    let userData=req.body;
    producerController.updateProducer(req.params.id,userData).then(data=>{
        res.send(data);
    }).catch(err=>{
        res.send(err);
    });
})


function renderProducerListPage(req,res,next){
    producerController.getProducers().then(producers=>{
        res.render('admin/screens/producer-list',{"producers":producers});
    }).catch(err=>{
        res.send(err);
    })
}

module.exports=Router;
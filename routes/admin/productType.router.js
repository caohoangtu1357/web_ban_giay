const Router= require('express').Router();

const productTypeController=require('../../controller/productType.controller');



Router.post('/add-product-type',(req,res)=>{
    let userData= req.body;
    productTypeController.createProductType(userData).then(data=>{
        return res.send(data);
    }).catch(err=>{
        return res.send(err);
    });
});

Router.get('/get-product-type-list',(req,res,next)=>{
    productTypeController.getProductTypes().then(productTypes=>{
        return res.render('admin/screens/product-type-list',{"productTypes":productTypes});
    }).catch(err=>{
        res.send(err);
    });
});

Router.get('/get-product-type/:id',(req,res)=>{
    let userData=req.body;

    productTypeController.getProductType(req.params.id).then(data=>{
        return res.send(data);
    }).catch(err=>{
        return res.send(err);
    });
    
})

Router.delete('/delete-product-type/:id',(req,res)=>{
    let userData=req.body;
    productTypeController.deleteProductType(req.params.id).then(data=>{
        res.send(data);
    }).catch(err=>{
        res.send(err);
    });
});

Router.put('/update-product-type/:id',(req,res)=>{
    let userData=req.body;
    productTypeController.updateProductType(req.params.id,userData).then(data=>{
        res.send(data);
    }).catch(err=>{
        res.send(err);
    });
})

module.exports=Router;
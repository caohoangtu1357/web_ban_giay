
const Router =require('express').Router()
const productController=require('../../controller/product.controller');
const productTypeController=require('../../controller/productType.controller');
const producerController=require('../../controller/producer.controller');
const fs= require('fs');
const path=require('path');

Router.get('/add-product',function(req,res,next){
    producerController.getProducers().then(producers=>{
        productTypeController.getProductTypes().then(productTypes=>{
            res.render('admin/screens/add-product',{"producers":producers,"productTypes":productTypes});
        }).catch(err=>{
            res.send(err);
        })
    }).catch(err=>{
        res.send(err);
    })
});

Router.delete('/delete-an-thumbnail/:idProduct/:thumbnail',(req,res,next)=>{
    productController.deleteAnThumbnail(req.params.idProduct,req.params.thumbnail).then(deleted=>{
        res.json({"status":"ok"});
    }).catch(err=>{
        res.send(err);
    })
});

Router.post('/add-product',(req,res,next)=>{
    productController.createProduct(data=req.body,req.files.thumbnails).then(newProduct=>{
        res.send(newProduct);
    }).catch(err=>{
        res.send(err);
    })
    
});

Router.get('/get-products',(req,res)=>{

    productController.getProducts().then(data=>{
        return res.send(data);
    }).catch(err=>{
        res.send(err);
    });
    
    
});

Router.get('/get-product/:id',(req,res)=>{
    let userData=req.body;

    productController.getProduct(req.params.id).then(data=>{
        return res.send(data);
    }).catch(err=>{
        return res.send(err);
    });
    
})

Router.delete('/delete-product/:id',(req,res)=>{
    let userData=req.body;
    productController.deleteProduct(req.params.id).then(data=>{
        res.send(data);
    }).catch(err=>{
        res.send(err);
    });
});



Router.get('/update-product/:idProduct',(req,res,next)=>{
    producerController.getProducers().then(producers=>{
        productTypeController.getProductTypes().then(productTypes=>{
            try{
                productController.getProduct(req.params.idProduct).then(product=>{
                    res.render('admin/screens/update-product',{"producers":producers,"productTypes":productTypes,"product":product});
                }).catch(err=>{
                    res.send(err);
                })
            }catch(err){
                console.log(err);
            }
            
        }).catch(err=>{
            res.send(err);
        })
    }).catch(err=>{
        res.send(err);
    })
});

Router.get('/get-products-list-by-producer/:idProducer',(req,res)=>{
    productController.getProductsByIdProducer(req.params.idProducer).then(products=>{
        res.render('admin/screens/product-list',{"products":products});
    }).catch(err=>{
        res.send(err);
    });
});

Router.get('/get-products-list-by-product-type/:idProductType',(req,res)=>{
    productController.getProductsByIdProductType(req.params.idProductType).then(products=>{
        res.render('admin/screens/product-list',{"products":products});
    }).catch(err=>{
        res.send(err);
    })
})


Router.post('/update-product',(req,res,next)=>{
    productController.updateProduct(req.body,req.files)
    .then(updated=>{
        res.redirect('/admin/product/get-products-list-by-product-type/'+updated.product_type_id);
    }).catch(err=>{
        console.log(err,"asdsadasd");
        res.send(err);
    })
});


module.exports=Router;
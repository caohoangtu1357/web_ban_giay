const Router= require('express').Router();


const introductionController=require('../../controller/introduction.controller');


Router.post('/add-introduction',(req,res)=>{
    let userData= req.body;
    introductionController.createIntroduction(userData).then(data=>{
        return res.send(data);
    }).catch(err=>{
        return res.send(err);
    });
})

Router.get('/get-introduction',(req,res)=>{

    introductionController.getIntroduction().then(data=>{
        return res.send(data);
    }).catch(err=>{
        res.send(err);
    });
    
    
});


Router.delete('/delete-introduction/:id',(req,res)=>{
    let userData=req.body;
    introductionController.deleteIntroduction(req.params.id).then(data=>{
        res.send(data);
    }).catch(err=>{
        res.send(err);
    });
});

Router.put('/update-introduction/:id',(req,res)=>{
    let userData=req.body;
    introductionController.updateIntroduction(req.params.id,userData).then(data=>{
        res.send(data);
    }).catch(err=>{
        res.send(err);
    });
})

module.exports=Router;
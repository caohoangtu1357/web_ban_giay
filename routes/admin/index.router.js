const Router=require('express').Router();


Router.get('/dashboard',renderAdminDashboard);
//Router.use('/',require('./users.router'));
Router.use('/producer',require('./producer.router'));
Router.use('/product',require('./product.router'));
Router.use('/introduction',require('./introduction.router'));
Router.use('/product-type',require('./productType.router'));
Router.use('/reference',require('./reference.router'));



function renderAdminDashboard(req,res,next){
    return res.render('admin/screens/dashboard');
}

module.exports=Router;
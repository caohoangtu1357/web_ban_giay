const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const introductionSchema=new Schema({
    name:
        {
            type:String, 
            required:true
        },
    description:
        {
            type:String, 
            required:true
        },
    phone:
        {
            type:String, 
            required:true
        },
    social_link:
    [
        {
            name:
            {
                type:String, 
                required:true
            },
            link:
            {
                type:String, 
                required:true
            }
        }
    ]
        
});

const introductions=mongoose.model("introductions",introductionSchema);
module.exports=introductions;
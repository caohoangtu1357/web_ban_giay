const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const producerSchema=new Schema({
    name:
        {
            type:String, 
            required:true
        }
});

const producers=mongoose.model("producers",producerSchema);
module.exports=producers;
const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const productSchema=new Schema({
    name:
        {
            type:String, 
            required:true
        },
    price:
        {
            type:Number, 
            required:true
        },
    discount:
        {
            type:Number,
            default: 0
        },
    promotion:
        {
            type:String
        },
        
    sizes:
        [

        ],
    status:
        {
            type:String, 
            default: 'con_hang'
        },
    product_type_id:
        {
            type:Schema.Types.ObjectId,
            ref:'Product_types'
        },
    producer_id:
        {
            type:Schema.Types.ObjectId,
            ref:'producers'
        },
    gender:
        {
            type:String, 
            required:true
        },
    description:
        {
            type:String, 
            required:true
        },
    thumbnail:
        [
        
        ]
    
        
});

const products = mongoose.model("products",productSchema);
module.exports=products;
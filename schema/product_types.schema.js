const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const productTypeSchema=new Schema({
    name:
        {
            type:String, 
            required:true
        }
});

const productTypes=mongoose.model("product_type",productTypeSchema);
module.exports=productTypes;
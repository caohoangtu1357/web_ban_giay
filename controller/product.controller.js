const product=require('../model/products.model');
const path=require('path');
const fs=require('fs');

function getSpecific(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function getProductsByIdProductType(idProductType){
    return new Promise((resolve,reject)=>{
        product.getProductsByIdProductType(idProductType).then(products=>{
            resolve(products);
        }).catch(err=>{
            reject(err);
        });
    });
    
}

function getProductsByIdProducer(idProducer){
    return new Promise((resolve,reject)=>{
        product.getProductsByIdProducer(idProducer).then(products=>{
            resolve(products);
        }).catch(err=>{
            reject(err);
        });
    });
}

function createProduct(data,thumbnails){
    
    var thumbnails_name=Array();

    if(typeof thumbnails.length==="undefined"){
        thumbnails_name.push(getSpecific(30)+thumbnails.name);
    }else{
        for(let i=0;i<thumbnails.length;i++){
            var new_file_name=getSpecific(30)+thumbnails[i].name;
            thumbnails_name.push(new_file_name);
        }
    }

    var str=data.sizes.replace(new RegExp(" ", "g"), '');
    var sizes=str.split(",");

    return new Promise((resolve,reject)=>{
        product.setProduct(
            data.name,
            data.price,
            data.promotion,
            sizes,
            data.status,
            data.product_type_id,
            data.gender,
            data.description,
            thumbnails_name,
            data.producer_id,
            data.discount
        ).then(newProduct=>{
            try{
                if(thumbnails_name.length===1){
                    file.mv(path.join(__dirname,'../public/upload/product_thumbnails/'+thumbnails_name[0]),function(errImage){
                        if(errImage){
                            console.log(errImage);
                        }else{
                            console.log("success");
                        }
                    });
                }else{
                    for(let i=0;i<thumbnails.length;i++){
                        thumbnails[i].mv(path.join(__dirname,'../public/upload/product_thumbnails/'+thumbnails_name[i]),function(errImage){
                            if(errImage){
                                console.log(errImage);
                            }else{
                                console.log("success");
                            }
                        });
                    }
                }
                return resolve(newProduct);
            }catch(errWhenMoveFile){
                console.log(errWhenMoveFile);
            }
        }).catch(err=>{
            return reject(err);
        })
    });
}


function getProducts(){
    return new Promise((resolve,reject)=>{
        product.gets().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

function getProduct(id){
    return new Promise((resolve,reject)=>{
        product.get(id).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

function deleteProduct(id){
    return new Promise((resolve,reject)=>{
        product.deleteProduct(id).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

function updateProduct(data,files){
    console.log(files);
    var file=files;

    
        console.log(file);
        var array_name=Array();
        console.log(file);
        if(file!==null){
            var file=file.thumbnail;
            if(typeof file.length==="undefined"){
                array_name.push(getSpecific(20)+file.name);
            }else{
                for(let i=0;i<file.length;i++){
                    var new_file_name=getSpecific(20)+file[i].name;
                    array_name.push(new_file_name);
                }
            }
        }

        var str=data.sizes.replace(new RegExp(" ", "g"), '');
        var sizes=str.split(",");

        return new Promise((resolve,reject)=>{
            product.get(data.id).then(oldProduct=>{
                product.updateProduct(
                    data.id,
                    data.name,
                    data.price,
                    data.promotion,
                    sizes,
                    data.status,
                    data.product_type_id,
                    data.gender,
                    data.description,
                    [...oldProduct.thumbnail, ...array_name],
                    data.producer_id,
                    data.discount,
                ).then(newProduct=>{
                    if(array_name.length>0){
                        if(array_name.length===1){
                            file.mv(path.join(__dirname,'../public/upload/product_thumbnails/'+array_name[0]),function(err){
                                if(err){
                                    console.log(err);
                                }else{
                                    console.log("success");
                                }
                            });
                        }else{
                            for(let i=0;i<file.length;i++){
                                file[i].mv(path.join(__dirname,'../public/upload/product_thumbnails/'+array_name[i]),function(err){
                                    if(err){
                                        console.log(err);
                                    }else{
                                        console.log("success");
                                    }
                                });
                            }
                        }
                    }
        
                    return resolve(newProduct);
                }).catch(err=>{
                    return reject(err);
                });
            }).catch(err=>{
                console.log(err);
            })
            
        });

        
    

    
}

function deleteAnThumbnail(idProduct,thumbnail){
    return new Promise((resolve,reject)=>{
        product.deleteAnThumbnail(idProduct,thumbnail).then(deleted=>{
            fs.unlink(path.join(__dirname, '../public/upload/product_thumbnails/')+thumbnail,(err)=>{
                if(err){
                    reject(err);
                }else{
                    resolve(deleted);
                }
            });
        }).catch(err=>{
            reject(err);
        })
    });
}




module.exports={
    createProduct:createProduct,
    getProducts:getProducts,
    getProduct:getProduct,
    deleteProduct:deleteProduct,
    updateProduct:updateProduct,
    getProductsByIdProductType:getProductsByIdProductType,
    getProductsByIdProducer:getProductsByIdProducer,
    deleteAnThumbnail:deleteAnThumbnail
}
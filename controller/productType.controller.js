var productType= require('../model/product_types.model');

function createProductType(data){
    return new Promise((resolve,reject)=>{
        productType.setProductType(data.name).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

function getProductTypes(){
    return new Promise((resolve,reject)=>{
        productType.gets().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        });
    });
}

function deleteProductType(id){
    return new Promise((resolve,reject)=>{
        productType.deleteProductType(id).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

function getProductType(id){
    return new Promise((resolve,reject)=>{
        productType.getProductType(id).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

function updateProductType(id,data){
    return new Promise((resolve,reject)=>{
        productType.updateProductType(id,data.name).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

module.exports={
    createProductType:createProductType,
    getProductType:getProductType,
    deleteProductType:deleteProductType,
    getProductTypes:getProductTypes,
    updateProductType:updateProductType
}
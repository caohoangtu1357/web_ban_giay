var introduction=require('../model/introduction.model');


function createIntroduction(data){
    return new Promise((resolve,reject)=>{
        introduction.setIntroduction(data.name,data.description,data.phone,data.social_link).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

function getIntroductions(){
    return new Promise((resolve,reject)=>{
        introduction.gets().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        });
    });
}

function deleteIntroduction(id){
    return new Promise((resolve,reject)=>{
        introduction.deleteIntroduction(id).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

function updateIntroduction(id,data){
    return new Promise((resolve,reject)=>{
        introduction.update(id,data.name,data.description,data.phone,data.social_link).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

module.exports={
    createIntroduction:createIntroduction,
    deleteIntroduction:deleteIntroduction,
    getIntroductions:getIntroductions,
    updateIntroduction:updateIntroduction
}
var producer=require('../model/producers.model');


function createProducer(data){
    return new Promise((resolve,reject)=>{
        producer.setProducer(data.name).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    })
}

function getProducers(){
    return new Promise((resolve,reject)=>{
        producer.gets().then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        });
    });
}

function deleteProducer(id){
    return new Promise((resolve,reject)=>{
        producer.deleteProducer(id).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

function getProducer(id){
    return new Promise((resolve,reject)=>{
        producer.getProducer(id).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

function updateProducer(id,data){
    return new Promise((resolve,reject)=>{
        producer.update(id,data.name).then(data=>{
            return resolve(data);
        }).catch(err=>{
            return reject(err);
        })
    });
}

module.exports={
    createProducer:createProducer,
    getProducer:getProducer,
    deleteProducer:deleteProducer,
    getProducers:getProducers,
    updateProducer:updateProducer
}
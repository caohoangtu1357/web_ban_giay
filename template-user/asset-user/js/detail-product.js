let myImgSlide=document.getElementsByClassName("img-show-slide");
let countImgShow=0;

defaultImgShow();

function defaultImgShow(){
    for(let i=0;i<myImgSlide.length;i++){
        myImgSlide[i].style.display="none";
    }
    myImgSlide[0].style.display="block";
}

/* control-img  */
let myImgControl=document.getElementsByClassName("img-show-control");


let countImgControlLeft=0;
let countImgControlRight=3;

defaultImgControl();

function defaultImgControl(){
    for(let i=4;i<myImgControl.length;i++){
        myImgControl[i].style.display="none";
    }
}

function clickImgControl(number){
    myImgSlide[countImgShow].style.display="none";
    myImgSlide[number].style.display="block";
    countImgShow=number;
}

document.getElementById("left").addEventListener("click", function(){
    if(countImgControlLeft==0){
        return;
    }
    countImgControlLeft--;
    myImgControl[countImgControlRight].style.display="none";
    countImgControlRight--;
    for(let i=countImgControlLeft;i<=countImgControlRight;i++){
        myImgControl[i].style.display="inline-block";
    }
})

document.getElementById("right").addEventListener("click", function(){
    if(countImgControlRight==myImgControl.length-1){
        return;
    }
    for(let i=0;i<myImgControl.length;i++){
        myImgControl[i].style.display="none";
    }
    countImgControlRight++;
    countImgControlLeft++;
    for(let i=countImgControlLeft;i<=countImgControlRight;i++){
        myImgControl[i].style.display="inline-block";
    }
})

/* description-size */
let myDesSize=document.getElementsByClassName("des-size");


defaultDesSize();

function defaultDesSize(){
    for(let i=0;i<myDesSize.length;i++){
        myDesSize[i].style.display="none";
    }
    myDesSize[0].style.display="block";
    document.getElementById("description-sup").style.textDecoration="underline";
}

document.getElementById("description-sup").addEventListener("click",function(){
    myDesSize[1].style.display="none";
    myDesSize[0].style.display="block";
    document.getElementById("description-sup").style.textDecoration="underline";
    document.getElementById("size-sup").style.textDecoration="none";

})

document.getElementById("size-sup").addEventListener("click",function(){
    myDesSize[0].style.display="none";
    myDesSize[1].style.display="block";
    document.getElementById("size-sup").style.textDecoration="underline";
    document.getElementById("description-sup").style.textDecoration="none";
})
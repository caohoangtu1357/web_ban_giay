
exports.checkAuth=function(req,res,next){
    return function(req,res,next){
        if(!req.user){
            return res.json({status:"user not logged in"})
        }
        next();
    }
}